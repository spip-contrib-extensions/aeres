<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}


/* pour que le pipeline ne rale pas ! */
function aeres_autoriser(){}

/**
 * Autorisation de v�rifier les r�f�rences biblio
  *
 * @param string $faire : l'action � faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant num�rique de l'objet
 * @param array $qui : les �l�ments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true/false : true si autoris�, false sinon
 */

function autoriser_aeresbiblio_verifier_dist($faire, $type, $id, $qui, $opt){
	$autorise = false;

	include_spip('inc/config');
	$type = lire_config('aeres/autorisation_verif_type');
	if($type){
		switch($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement sp�cifique pour la valeur 'tous'
				if(in_array('tous',lire_config('aeres/autorisation_verif_statuts',array('0minirezo')))){
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('aeres/autorisation_verif_statuts',array()));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('aeres/autorisation_verif_auteurs',array()));
				break;
		}
		if($autorise == true){
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	// Si pas configur� ou pas autoris� dans la conf => webmaster
	$autorise = ($qui['webmestre'] == 'oui');

	return $autorise;
}

function autoriser_bibliounite_voir_dist($faire, $type, $id, $qui, $opt){
	$autorise = false;

	include_spip('inc/config');
	$type = lire_config('aeres/autorisation_biblio_unite_type');
	if($type){
		switch($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement sp�cifique pour la valeur 'tous'
				if(in_array('tous',lire_config('aeres/autorisation_biblio_unite_statuts',array('0minirezo')))){
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('aeres/autorisation_biblio_unite_statuts',array()));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('aeres/autorisation_biblio_unite_auteurs',array()));
				break;
		}
		if($autorise == true){
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	// Si pas configur� ou pas autoris� dans la conf => webmaster
	$autorise = ($qui['webmestre'] == 'oui');

	return $autorise;
}

function autoriser_aeresstat_voir_dist($faire, $type, $id, $qui, $opt){
	$autorise = false;

	include_spip('inc/config');
	$type = lire_config('aeres/autorisation_stats_type');
	if($type){
		switch($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement sp�cifique pour la valeur 'tous'
				if(in_array('tous',lire_config('aeres/autorisation_stats_statuts',array('0minirezo')))){
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('aeres/autorisation_stats_statuts',array()));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('aeres/autorisation_stats_auteurs',array()));
				break;
		}
		if($autorise == true){
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	// Si pas configur� ou pas autoris� dans la conf => webmaster
	$autorise = ($qui['webmestre'] == 'oui');

	return $autorise;
}


