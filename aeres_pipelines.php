<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function aeres_affiche_milieu($flux) {
	$exec = $flux['args']['exec'];
	if ($exec=='configurer_zotspip' && autoriser('webmestre')) {
		$flux['data'] .= recuperer_fond('prive/inclure/configurer_aeres');
	} 

	return $flux;
}

function aeres_affiche_droite($flux) {
	$exec = $flux['args']['exec'];
	if ($exec=='ticket') {
		$flux['data'] .= '<h3>Correspondances Zotero&nbsp;/&nbsp;HCERES</h3><a href="./?exec=infos_aeres" class="mediabox">Tableau détaillé</a>';
		//.recuperer_fond('inclure/correspondances_zotero_aeres_details');
	}
	return $flux;
}

